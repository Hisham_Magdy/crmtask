@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Customer:</div>

                    <div class="panel-body">
                        <form action="{{route('saveCustomers')}}" method="post">
                            {{csrf_field()}}

                            <div class="form-group {{$errors->has('name') ? ' has-error' : ' ' }}">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Employee Name" value="{{ old('name') }}">
                                @if($errors->has('email'))<span>{{$errors->first('name')}}</span>@endif
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
