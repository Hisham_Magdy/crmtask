@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Actions of: {{$customer->name}} <a href="{{route('addAction', $customer->id)}}" class="btn btn-xs btn-primary" style="float: right">Add</a></div>

                    <div class="panel-body">
                        @if(count($customer->action) > 0)
                            <div id="accordion" role="tablist" aria-multiselectable="true">

                                @foreach($customer->action as $action)
                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{$action->id}}" aria-expanded="true" aria-controls="collapseOne">
                                                    <strong>Employee name</strong>: {{$action->employee->name}}. <br> <strong>Action</strong>: @if($action->type == 1) Call @elseif($action->type == 2) Visit @else Folow up @endif <br> <strong>Date</strong>: {{$action->created_at->diffForHumans()}}
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="collapseOne{{$action->id}}" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="card-block">
                                                {{$action->result}}
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach

                            </div>
                        @else
                            Sorry, there is no data to show!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
