@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Customers: <a href="{{route('addCustomers')}}" class="btn btn-xs btn-primary" style="float: right">Add</a></div>

                    <div class="panel-body">
                        @if(count($customers) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Edit/Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <div name="app">
                                    @foreach($customers as $customer)
                                        <tr>
                                            <td>{{$customer->name}}</td>
                                            <td>
                                                <a href="{{route('editCustomers', $customer->id)}}" class="btn btn-xs btn-default">Edit</a>
                                                <a data-toggle="modal" data-target="#myModal{{$customer->id}}" class="btn btn-xs btn-{{$customer->is_active == true ? 'danger' : 'primary'}}">{{$customer->is_active == true ? 'Delete' : 'Active'}}</a>
                                            </td>
                                        </tr>
                                        {{--Modal Start--}}
                                        <div class="modal fade" id="myModal{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Change Customer Activity:</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure to {{$customer->is_active == true ? '"Delete"' : '"Active"'}} {{$customer->name}}?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form action="{{route('updateEmployees', $customer->id)}}" method="post">
                                                            {{csrf_field()}}
                                                            <button type="submit" class="btn btn-primary">Yes</button>
                                                            <a class="btn btn-secondary" data-dismiss="modal">No</a>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--Modal End--}}
                                    @endforeach
                                </div>
                                </tbody>
                            </table>
                            <div>
                                {{$customers->links()}}
                            </div>
                        @else
                            Sorry, there is no data to show!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
