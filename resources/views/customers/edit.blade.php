@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Customer: "{{$customer->name}}"</div>

                    <div class="panel-body">
                        <form action="{{route('updateCustomers', $customer->id)}}" method="post">
                            {{csrf_field()}}

                            <div class="form-group {{$errors->has('name') ? ' has-error' : ' ' }}">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Customer Name" value="{{ $customer->name }}">
                                @if($errors->has('name'))<span>{{$errors->first('name')}}</span>@endif
                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
