@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Customers of: "{{$employee->name}}" <a href="{{route('assignCustomer', $employee->id)}}" class="btn btn-xs btn-primary" style="float: right">Asign Customers</a></div>

                    <div class="panel-body">
                        @if(count($employee->customer) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Log/Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <div name="app">
                                    @foreach($employee->customer as $customer)
                                        <tr>
                                            <td>{{$customer->customer->name}}</td>
                                            <td>
                                                <a href="{{route('showCustomerLog', $customer->id)}}" class="btn btn-xs btn-default">Show Log</a>
                                                <a data-toggle="modal" data-target="#myModal{{$customer->id}}" class="btn btn-xs btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                        {{--Modal Start--}}
                                        <div class="modal fade" id="myModal{{$customer->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Change Customer Activity:</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure to Delete "{{$customer->customer->name}}" form list?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form action="{{route('deleteAssignCustomer', $customer->customer->id)}}" method="post">
                                                            {{csrf_field()}}
                                                            <button type="submit" class="btn btn-primary">Yes</button>
                                                            <a class="btn btn-secondary" data-dismiss="modal">No</a>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--Modal End--}}
                                    @endforeach
                                </div>
                                </tbody>
                            </table>
                        @else
                            Sorry, there is no data to show!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
