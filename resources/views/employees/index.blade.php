@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Emplyees: <a href="{{route('addEmplyees')}}" class="btn btn-xs btn-primary" style="float: right">Add</a></div>

                    <div class="panel-body">
                        @if(count($employees) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Edit/Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($employees as $employee)
                                        <tr>
                                            <td>{{$employee->name}}</td>
                                            <td>{{$employee->email}}</td>
                                            <td><a href="{{route('editEmployees', $employee->id)}}" class="btn btn-xs btn-default">Edit</a>
                                                /
                                                <a data-toggle="modal" data-target="#myModal{{$employee->id}}" class="btn btn-xs btn-{{$employee->is_active == true ? 'danger' : 'primary'}}">{{$employee->is_active == true ? 'Delete' : 'Active'}}</a>
                                                /
                                                <a href="{{route('EmployeeCustomers', $employee->id)}}" class="btn btn-xs btn-primary">Show Customers</a>
                                            </td>
                                        </tr>
                                        {{--Modal Start--}}
                                        <div class="modal fade" id="myModal{{$employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Change Employee Activity:</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure to {{$employee->is_active == true ? '"Delete"' : '"Active"'}} {{$employee->name}}?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <form action="{{route('updateEmployees', $employee->id)}}" method="post">
                                                            {{csrf_field()}}
                                                            <button type="submit" class="btn btn-primary">Yes</button>
                                                            <a type="button" class="btn btn-secondary" data-dismiss="modal">No</a>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--Modal End--}}
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            Sorry, there is no data to show!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
