@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">My Profile:</div>

                    <div class="panel-body">
                        @if(count($customers) > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Show Log/Add Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <div name="app">
                                    @foreach($customers as $customer)
                                        @if($customer->customer->is_active == true)
                                            <tr>
                                                <td>{{$customer->customer->name}}</td>
                                                <td>
                                                    <a href="{{route('showCustomerLog', $customer->customer->id)}}" class="btn btn-xs btn-default">Show Log</a>
                                                    <a href="{{route('addAction', $customer->customer->id)}}" class="btn btn-xs btn-primary">Add Action</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </div>
                                </tbody>
                            </table>
                        @else
                            Sorry, there is no data to show!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
