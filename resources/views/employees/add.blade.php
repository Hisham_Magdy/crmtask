@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Employee:</div>

                    <div class="panel-body">
                        <form action="{{route('saveEmployees')}}" method="post">
                            {{csrf_field()}}

                            <div class="form-group {{$errors->has('name') ? ' has-error' : ' ' }}">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Employee Name" value="{{ old('name') }}">
                                @if($errors->has('email'))<span>{{$errors->first('name')}}</span>@endif
                            </div>

                            <div class="form-group {{$errors->has('email') ? ' has-error' : ' ' }}">
                                <label for="email">Email :</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter Employee Email" value="{{ old('email') }}">
                                @if($errors->has('email'))<span>{{$errors->first('email')}}</span>@endif
                            </div>

                            <div class="form-group {{$errors->has('password') ? ' has-error' : ' ' }}">
                                <label for="password">Password:</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter Employee Password">
                                @if($errors->has('password'))<span>{{$errors->first('password')}}</span>@endif
                            </div>

                            <div class="form-group">
                                <label for="password"> Confirm Password:</label>
                                <input type="password" class="form-control" id="password" name="password_confirmation" placeholder="Enter Employee Confirm Password">
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
