@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Assign Customer to: "{{$employee->name}}"</div>

                    <div class="panel-body">
                        <form action="{{route('saveAssignCustomer', $employee->id)}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($customers as $customer)
                                        <div class="col-md-3">
                                            <input type="checkbox" value="{{$customer->id}}" name="customers[]"> {{$customer->name}}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
