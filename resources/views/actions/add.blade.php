@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add action to: "{{$customer->customer->name}}"</div>

                    <div class="panel-body">
                        <form action="{{route('saveAction', $customer->id)}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group {{$errors->has('action_type') ? ' has-error' : ' ' }}">
                                <label for="action_type">Action Type:</label>
                                <select type="text" class="form-control" id="action_type" name="action_type">
                                    <option value="1">Call</option>
                                    <option value="1">Visit</option>
                                    <option value="1">Folow up</option>
                                </select>
                                @if($errors->has('action_type'))<span>{{$errors->first('action_type')}}</span>@endif
                            </div>

                            <div class="form-group {{$errors->has('result') ? ' has-error' : ' ' }}">
                                <label for="action_type">Result:</label>
                                <textarea type="text" class="form-control" id="result" name="result">{{old('result')}}</textarea>
                                @if($errors->has('result'))<span>{{$errors->first('result')}}</span>@endif
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
