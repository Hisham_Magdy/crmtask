<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect()->route('login');
});

Auth::routes();
//Route::group(['middleware' => 'auth:web,auth:employee' ], function ()
//{
    Route::get('/home', 'HomeController@index')->name('home');
#Employees
    Route::get('/employees', 'EmployeesController@index')->name('employees');
    Route::get('/employees/add', 'EmployeesController@add')->name('addEmplyees');
    Route::post('/employees/save', 'EmployeesController@save')->name('saveEmployees');
    Route::get('/employees/edit/{id}', 'EmployeesController@edit')->name('editEmployees');
    Route::post('/employees/update/{id}', 'EmployeesController@update')->name('updateEmployees');
    Route::post('/employees/activity/{id}', 'EmployeesController@activity')->name('activityEmployees');

#Customers
    Route::get('/customers', 'CustomersController@index')->name('customers');
    Route::get('/customers/add', 'CustomersController@add')->name('addCustomers');
    Route::post('/customers/save', 'CustomersController@save')->name('saveCustomers');
    Route::get('/customers/edit/{id}', 'CustomersController@edit')->name('editCustomers');
    Route::post('/customers/update/{id}', 'CustomersController@update')->name('updateCustomers');
    Route::post('/customers/activity/{id}', 'CustomersController@activity')->name('activityCustomers');
    Route::get('/customers/{id}/log', 'CustomersController@log')->name('showCustomerLog');

#show Employee & Assign Customers
    Route::get('/employee/{id}/customers', 'AssignCustomersController@index')->name('EmployeeCustomers');
    Route::get('employees/{id}/assign', 'AssignCustomersController@add')->name('assignCustomer');
    Route::post('employees/{id}/assign/save', 'AssignCustomersController@save')->name('saveAssignCustomer');
    Route::post('employees/{id}/assign/delete', 'AssignCustomersController@delete')->name('deleteAssignCustomer');

#Employee login
    Route::get('/employee/login', 'EmployeeLoginController@index')->name('loginFormEmployee');
    Route::post('/employee/attempt', 'EmployeeLoginController@login')->name('loginEmployee');
    Route::post('/employee/logout', 'EmployeeLoginController@logout')->name('logoutEmployee');

#Actions
    Route::get('customer/{id}/action/add', 'ActionsController@add')->name('addAction');
    Route::post('customer/{id}/action/save', 'ActionsController@save')->name('saveAction');

#Profile
    Route::get('employee/profile', 'ProfileController@index')->name('employeeProfile');
