<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;

class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $employees = Employee::paginate(30);
        return view('employees.index', compact('employees'));
    }

    public function add()
    {
        return view('employees.add');
    }

    public function save(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:employees',
            'password' => 'required|min:6|confirmed',
        ]);

        Employee::create([
            'name' => $request->name,
            'email' => $request->email,
            'is_active' => true,
            'password' => bcrypt($request->password),
        ]);

        Flashy::success('Employee has been added successfully');
        return redirect()->route('employees');
    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        if(!$employee)
            abort(404);
        return view('employees.edit', compact('employee'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:employees',
            'password' => 'confirmed',
        ]);


        $employee = Employee::find($id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->has('password') ? bcrypt($request->password): '',
        ]);;

        Flashy::success('Employee has been updated successfully');
        return redirect()->route('employees');
    }

    public function activity($id)
    {
        $employee = Employee::find($id);
        if($employee->is_active == true){
            $employee->update(['is_active', false]);
        } else {
            $employee->update(['is_active', true]);
        }
        Flashy::success('Employee activity has been updated');
        return redirect()->back();
    }

}
