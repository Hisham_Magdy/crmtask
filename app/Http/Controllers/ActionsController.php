<?php

namespace App\Http\Controllers;

use App\Action;
use App\Customer;
use App\Employee_Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MercurySeries\Flashy\Flashy;

class ActionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:employee']);
    }

    public function add($id)
    {
        $customer = Employee_Customer::where('customer_id',$id)->where('employee_id',Auth::user()->id)->first();
        if(!$customer || !$customer->customer || $customer->customer->is_active == false)
            abort(404);
        return view('actions.add', compact('customer'));
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            'action_type' => 'required',
            'result' => 'required|max:255',
        ]);

        Action::create([
            'type' => $request->action_type,
            'customer_id' => $id,
            'employee_id' => Auth::user()->id,
            'result' => $request->result,
        ]);

        Flashy::success('Action has added successfully');
        return redirect()->route('showCustomerLog', $id);
    }
}
