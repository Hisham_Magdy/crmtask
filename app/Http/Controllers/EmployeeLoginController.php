<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }

    public function index()
    {
        return view('employees.login');
    }

    public function login(Request $request)
    {
        if(Auth::guard('employee')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return redirect()->route('home');
        }

    }

    public function logout()
    {
        Auth::guard('employee')->logout();
        return redirect()->route('loginFormEmployee');
    }




}
