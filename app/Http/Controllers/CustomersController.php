<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;

class CustomersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:employee,web']);
    }

    public function index()
    {
        $customers = Customer::paginate(30);
        return view('customers.index', compact('customers'));
    }

    public function add()
    {
        return view('customers.add');
    }

    public function save(Request $request)
    {
        $this->validate($request, ['name']);

        Customer::create(['name' => $request->name]);

        Flashy::success('Customer has been added successfully');
        return redirect()->route('customers');
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        if(!$customer)
            abort(404);
        return view('customers.edit', compact('customer'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, ['name']);
        Customer::find($id)
            ->update(['name' => $request->name]);

        Flashy::success('Customer has been updated successfully');
        return redirect()->route('customers');
    }

    public function activity($id)
    {
        $customer = Customer::find($id);

        if($customer->is_active == true){
            $customer->update(['is_active', false]);
        } else {
            $customer->update(['is_active', true]);
        }

        Flashy::success('Customer activity has been updated');
        return redirect()->back();
    }
    public function log($id)
    {
        $customer = Customer::find($id);
        if(!$customer)
            abort(404);
        return view('customers.log', compact('customer'));
    }
}
