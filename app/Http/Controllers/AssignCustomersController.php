<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Employee;
use App\Employee_Customer;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;

class AssignCustomersController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:web']);
    }

    public function index($id)
    {
        $employee = Employee::find($id);
        if(!$employee)
            abort(404);
        return view('employees.customers.index', compact('employee'));
    }

    public function add($id)
    {
        $employee = Employee::find($id);

        if(!$employee) {
            abort(404);
        }
        elseif($employee->is_active == false){
            Flashy::warning('The Employee is not active!');
            return redirect()->back();
        }

        $customers = Customer::where('is_active', 1)->whereNotIn('id' , $employee->customer)->get();
        return view('assign.add', compact('employee', 'customers'));
    }

    public function save(Request $request, $id)
    {
        if(count($request->customers) > 0)
        {
            foreach ($request->customers as $customer) {
                Employee_Customer::create([
                    'customer_id' => $customer,
                    'employee_id' => $id
                ]);
            }
            Flashy::success('Customers has been asigned successfully');
            return redirect()->route('employees');
        }
        Flashy::warning('You have choose at least one customer!');
        return redirect()->back();
    }

    public function delete($id)
    {
        Employee_Customer::destroy($id);

        Flashy::success('Customer has been deleted successfully');
        return redirect()->back();
    }
}
