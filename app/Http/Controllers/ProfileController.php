<?php

namespace App\Http\Controllers;

use App\Employee_Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:employee']);
    }

    public function index()
    {
        $customers = Employee_Customer::where('employee_id', Auth::guard('employee')->user()->id)->get();

        return view('employees.profile', compact('customers'));
    }
}
