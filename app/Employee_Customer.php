<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_Customer extends Model
{
    protected $table = 'employees_customers';

    protected $fillable = ['employee_id', 'customer_id'];

    public function employee()
    {
        return $this->belongsTo('App\Employee', 'employee_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
}
