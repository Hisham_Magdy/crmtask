<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    use Notifiable;

    protected $table = 'employees';

    protected $fillable = ['name', 'email', 'is_active', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function customer()
    {
        return $this->hasMany('App\Employee_Customer', 'employee_id');
    }

    public function action()
    {
        return $this->hasMany('App\Action', 'employee_id');
    }

}
