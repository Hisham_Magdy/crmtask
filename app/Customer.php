<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = ['name'];

    public function employee()
    {
        return $this->hasMany('App\Employee_Customer', 'customer_id');
    }

    public function action()
    {
        return $this->hasMany('App\Action', 'customer_id');
    }
}
